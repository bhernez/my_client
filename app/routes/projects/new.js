import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		// regresar el modelo de la ruta
		return this.store.createRecord('project');
	},

	actions:{
		componentDidSave:function(){
			this.transitionTo('projects');
		},

		componentDidCancel:function(){
			this.transitionTo('projects');
		}
	}
}
);
