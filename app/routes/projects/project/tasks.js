import Ember from 'ember';

export default Ember.Route.extend({
	model: function(){
		var project = this.modelFor('projects.project');
		return project.get('tasks');
	}
});
