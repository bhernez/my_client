import Ember from 'ember';

export default Ember.Route.extend({
	beforeModel:function(){
		var project = this.modelFor('projects.project');
		this.set('project', project);
	},

	model:function(params){
		var project = this.get('project');
		return project.get('tasks').createRecord();
	}
});
