import Ember from 'ember';

export default Ember.Route.extend({
	isLoggedIn: function(){
		// Logica para saber si el usuario está logueado
		if(localStorage.authToken)
			return true;
		else
			return false;
	}.property(),

	beforeModel: function(transition){
		// Este método se ejecuta primero que todo en la ruta.
		// Aquí voy a evaluar si el usuario está o no logueado.
		if(!this.get('isLoggedIn'))
			this.transitionTo('login');
	}
});
