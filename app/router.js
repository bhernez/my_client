import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('projects', {path: '/'},function() {
    this.route('new', {});

    this.route('project', {path: ':project_id'}, function(){
      this.route('edit', {});
      this.route('tasks', {}, function(){
      	this.route('new')
      });
    });


  });
  this.route('login', {});
});

export default Router;
