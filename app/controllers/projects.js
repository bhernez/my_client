import Ember from 'ember';

export default Ember.Controller.extend({
	selectedProject: null,

	actions:{
		listProjectsClickHandler: function(project){
			this.set('selectedProject', project);
			console.log('En el controlador: listProjectsClickHandler');
		}
	}
});
