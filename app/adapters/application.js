import DS from 'ember-data';

export default DS.RESTAdapter.extend({
	host: 'http://labweb.noblyn.com:3000',
	namespace: 'api/v1',

	headers: {
			'Authorization': 'Token token=' + localStorage.authToken
	}
});
