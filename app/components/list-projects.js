import Ember from 'ember';

export default Ember.Component.extend({
	actions:{
		clickHandler: function(project){
			this.sendAction('clickHandlerAction', project);
		},

		deleteProject: function(project){
			if(confirm('¿Estás seguro de eliminar este proyecto?'))
				project.destroyRecord().then(function(){
					this.sendAction('didDestroyProject', project)
				}.bind(this));
		}
	}
});
