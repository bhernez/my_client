import Ember from 'ember';

export default Ember.Component.extend({
	actions:{
		save: function(){
			this.get('task').save().then(function(taskFromServer){
				this.sendAction('taskDidSave', taskFromServer);
			}.bind(this));
		}
	}
});
