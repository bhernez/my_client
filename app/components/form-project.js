import Ember from 'ember';

export default Ember.Component.extend({
	project: null,

	actions:{
		saveProject:function(){
			// mandar a guardar al servidor el project del formulario
			// .save() regresa promise
			this.get('project').save().then(function(){
				alert('proyecto guardado');
				this.sendAction('didSave');
			}.bind(this));
		},

		cancelNew:function(){
			this.get('project').rollbackAttributes();
			this.sendAction('didCancel');
		}
	}
});
