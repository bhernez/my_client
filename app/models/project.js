import DS from 'ember-data';
// tasks_ids: [1,2,3,4]
// links: {
// 	findAll: url
// }
export default DS.Model.extend({
	name: DS.attr('string'),
	startDate: DS.attr('string'),
	endDate: DS.attr('string'),
	done: DS.attr('boolean'),

	tasks: DS.hasMany('task', {async: true})

	//has_many
	//belongs_to
});
