import DS from 'ember-data';
//Task(project_id: integer, created_at: datetime, updated_at: datetime)
export default DS.Model.extend({
  title: DS.attr('string'),
  dueDate: DS.attr('string'),
  completed: DS.attr('boolean'),

  project: DS.belongsTo('project')
});
