import DS from 'ember-data';

export default DS.RESTSerializer.extend({
	attrs: {
		dueDate: 'due_date',
		project: 'project_id'
	}
});
