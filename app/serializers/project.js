import DS from 'ember-data';

export default DS.RESTSerializer.extend({
	attrs: {
		startDate: 'start_date',
		endDate: 'end_date'
	}
});
